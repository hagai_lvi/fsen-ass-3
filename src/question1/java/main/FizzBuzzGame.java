package question1.java.main;

/**
 * Created by hagai_lvi on 12/22/14.
 */
public class FizzBuzzGame {

	public static final String DELIM = ", ";
	public static final String FIZZ = "Fizz";
	private static final String BUZZ = "Buzz";
	private int n = 0;

	public FizzBuzzGame(){
		this.n = n;
	}

	public static String play(int num) {
		StringBuilder builder = new StringBuilder();
		for (int i = 1 ; i <= num ; i++){
			builder.append(getRepresentation(i));

			if (i < num)
				builder.append(DELIM);
		}

		return builder.toString();
	}

	public static String getRepresentation(int n){
		if (n%3 != 0 && n%5 != 0)
			return Integer.toString(n);

		String res = "";
		if (n % 3 == 0 )
			res = res + FIZZ;
		if (n % 5 == 0 )
			res = res + BUZZ;

		return res;

	}
}
