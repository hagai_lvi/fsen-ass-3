package question1.java.test;

import junit.framework.Assert;
import junit.framework.TestCase;
import question1.java.main.FizzBuzzGame;

/**
 * Created by hagai_lvi on 12/22/14.
 */
public class FizzBuzzTest extends TestCase {

	public static final String UP_TO_1 = "1";
	public static final String UP_TO_3 = "1, 2, Fizz";
	public static final String UP_TO_5 = "1, 2, Fizz, 4, Buzz";
	public static final String UP_TO_9 = "1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz";
	public static final String UP_TO_10 = "1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz";
	public static final String UP_TO_15 = "1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, FizzBuzz";
	public static final String UP_TO_100 = "1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, FizzBuzz, 16, 17, Fizz, 19, Buzz, Fizz, 22, 23, Fizz, Buzz, 26, Fizz, 28, 29, FizzBuzz, 31, 32, Fizz, 34, Buzz, Fizz, 37, 38, Fizz, Buzz, 41, Fizz, 43, 44, FizzBuzz, 46, 47, Fizz, 49, Buzz, Fizz, 52, 53, Fizz, Buzz, 56, Fizz, 58, 59, FizzBuzz, 61, 62, Fizz, 64, Buzz, Fizz, 67, 68, Fizz, Buzz, 71, Fizz, 73, 74, FizzBuzz, 76, 77, Fizz, 79, Buzz, Fizz, 82, 83, Fizz, Buzz, 86, Fizz, 88, 89, FizzBuzz, 91, 92, Fizz, 94, Buzz, Fizz, 97, 98, Fizz, Buzz";


	//Equivalence class for all regular numbers
	public void testRegularNum(){
		Assert.assertEquals("1", FizzBuzzGame.getRepresentation(1));
		assertEquals("2",FizzBuzzGame.getRepresentation(2));
	}

	//Equivalence class for all n such that n%3 == 0
	public void testModolu3(){
		assertEquals("Fizz", FizzBuzzGame.getRepresentation(3));
		assertEquals("Fizz", FizzBuzzGame.getRepresentation(6));
	}

	//Equivalence class for all n such that n%5 == 0
	public void testModolu5(){
		assertEquals("Buzz", FizzBuzzGame.getRepresentation(5));
		assertEquals("Buzz", FizzBuzzGame.getRepresentation(10));
	}

	//Equivalence class for all n such that n%15 == 0
	public void testModolu15(){
		assertEquals("FizzBuzz", FizzBuzzGame.getRepresentation(15));
		assertEquals("FizzBuzz", FizzBuzzGame.getRepresentation(30));
	}

	// ******************************************************************
	// * From this point we are checking the full string and not just 	*
	// * handling a specific number.									*
	// ******************************************************************

	// Extreme case - smallest possible string
	public void testSmallestN(){;
		assertEquals(UP_TO_1, FizzBuzzGame.play(1));
	}

	// Extreme case - first fizz
	public void testOneFizz(){
		assertEquals(UP_TO_3, FizzBuzzGame.play(3));
	}

	// Extreme case - first buzz
	public void testOneBuzz(){
		assertEquals(UP_TO_5, FizzBuzzGame.play(5));
	}

	// Equivalence class for more than one fizz
	public void testFewFizz(){
		assertEquals(UP_TO_9, FizzBuzzGame.play(9));
	}

	// Equivalence class for more than one buzz
	public void testFewBuzz(){
		assertEquals(UP_TO_10, FizzBuzzGame.play(10));
	}

	// Extreme case - first fizzbuzz
	public void testFizzBuzz(){
		assertEquals(UP_TO_15, FizzBuzzGame.play(15));
	}

	// Extreme case - full game
	public void testFullFizzBuzz(){
		assertEquals(UP_TO_100, FizzBuzzGame.play(100));
	}
}
